#include "Utils.h"

InMemoryFile loadFileInMemory(std::string filePath)
{
    InMemoryFile loadedFile;
    std::ifstream file = std::ifstream(filePath, std::ios::binary);
    if (!file.is_open()) {
        reportLog("Error opening file", LogLevel::ERROR);
    }

    file.seekg(0, std::ios::end);
    loadedFile.len = file.tellg();
    file.seekg(0, std::ios::beg);

    loadedFile.content = malloc(loadedFile.len);
    if (!file.read((c8*) loadedFile.content, loadedFile.len)) {
        free(loadedFile.content);
        loadedFile.content = nullptr;
        loadedFile.len = 0;
        reportLog("Error reading file", LogLevel::ERROR);
    }
    return loadedFile;
}

bool isSubstring(std::string firstStr, std::string otherStr)
{
    if (firstStr.find(otherStr) != std::string::npos) {
        return true;
    }

    return false;
}