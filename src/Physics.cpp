#include "Physics.h"

b8 checkColision(sf::Rect<f32> rect, sf::Rect<f32> otherRect)
{
    return rect.intersects(otherRect);
}

b8 checkColision(sf::Rect<f32> rect, sf::Rect<f32> otherRect, sf::Rect<f32>& intersection)
{
    return rect.intersects(otherRect, intersection);
}

enum ColisionType {
    NONE,
    BOTTOM,
    TOP,
};

void updatePhysics(PhysicsBody* body, std::vector<sf::Rect<f32>*> colisionTiles)
{
    body->velocity += (GRAVITY_ACCELERATION * METERS_TO_WORLD_UNITS) * gameState.delta;
    f32 positionOffset = body->velocity * gameState.delta;

    b8 isJumping = body->velocity < 0.0f;
    ColisionType colisionType = ColisionType::NONE;

    for (auto tile : colisionTiles) {
        sf::Rect<f32> testRect = body->rect;
        testRect.left += positionOffset;

        sf::Rect<f32> colisionRect;
        checkColision(testRect, *tile, colisionRect);
        b8 isGoingToColide = colisionRect.height != 0.0f;
        if (isGoingToColide) {
            if (body->rect.top > tile->top) {
                colisionType = ColisionType::TOP;
            }
            else {
                colisionType = ColisionType::BOTTOM;
            }

            positionOffset = positionOffset - colisionRect.height;
            break;
        }
    }

    if (colisionType == ColisionType::NONE || !isJumping && colisionType == ColisionType::TOP || isJumping && colisionType == ColisionType::BOTTOM) {
        body->rect.top += positionOffset;
        return;
    }
    body->velocity = 0.0f;
}