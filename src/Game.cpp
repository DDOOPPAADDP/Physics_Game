#include "Game.h"

void Game::initialize()
{
    #pragma region window_creation
    std::vector<sf::VideoMode> videoModes = sf::VideoMode::getFullscreenModes();
    sf::VideoMode applyMode = videoModes[videoModes.size() / 2];
    // creates the window
    this->window.create(applyMode, gameState.windowTitle, sf::Style::Close); // this will open the window in floating mode and pick a mid resolution by default
    
    // init imgui
    #ifdef DEBUG
    ImGui::SFML::Init(window);
    ImGui::SFML::SetCurrentWindow(window);
    #endif
    #pragma endregion


    #pragma region resource_loading
    // loads a font
    if (!this->font.loadFromFile("assets/FunGames.ttf")) {
        reportLog("Error trying to load font", LogLevel::FATAL);
        exit(-1);  
    }

    ScalableObject object;

    // creates a player and load all it's resources (sprites and etc...)
    this->player.position = sf::Vector2f(0.0f, 0.0f);
    this->player.moveOffset = 0.0f;
    this->player.camera.setSize(static_cast<sf::Vector2f>(this->window.getSize()));

    const f32 frameTime = 0.08f;
    this->player.playerAnimations[PlayerAnimations::IDLE] = loadAnimatedSprite("assets/knight_character/_Idle.png", PLAYER_SPRITE_SIZE, frameTime);
    this->player.playerAnimations[PlayerAnimations::RUN] = loadAnimatedSprite("assets/knight_character/_Run.png", PLAYER_SPRITE_SIZE, frameTime);
    this->player.playerAnimations[PlayerAnimations::JUMP] = loadAnimatedSprite("assets/knight_character/_Jump.png", PLAYER_SPRITE_SIZE, frameTime);
    this->player.playerAnimations[PlayerAnimations::FALL] = loadAnimatedSprite("assets/knight_character/_Fall.png", PLAYER_SPRITE_SIZE, frameTime);

    for (u8 i = 0; i < PlayerAnimations::MAX_PLAYER_ANIMATIONS; i++) {
        object = {
            .object = &this->player.playerAnimations[i]->target,
            .scaleFactor = PLAYER_SPRITE_SCALE_FACTOR
        };
        this->scalableObjects.push_back(object);
    }

    // creates and the parallax background
    this->parallax = loadParallaxBackgroundFromDirectory("assets/mountains_background");
    this->parallax->setSize(PARALLAX_SPRITE_SIZE);
    for (auto parallaxLayer : this->parallax->layers) {
        object = {
            .object = &parallaxLayer->sprite,
            .scaleFactor = PARALLAX_SCALE_FACTOR
        };
        this->scalableObjects.push_back(object);
    }


    // testing stuff
    #ifdef DEBUG
    this->colisionTester.setSize(sf::Vector2f(1000.0f, 40.0f));
    this->colisionTester.setFillColor(sf::Color::Blue);
    this->colisionTester.setPosition(sf::Vector2f(0.0f, 400.0f));
    object = {
        .object = &this->colisionTester,
        .scaleFactor = PLAYER_SPRITE_SCALE_FACTOR
    };
    this->scalableObjects.push_back(object);
    this->colisionTiles.push_back(&this->colisionTesterBounds);
    #endif

    #pragma endregion
}

void Game::update(sf::Time elapsedTime)
{
    updateControls(player);
    this->parallax->updateScrolling(this->player.camera.getCenter().x);
    this->player.updateCurrentAnimation(elapsedTime);
    this->player.updatePosition(this->colisionTiles);
    this->player.updateCamera(window);
    this->regularView = this->window.getDefaultView(); // this may cause performance issues

    #ifdef DEBUG
    // debug UI update
    ImGui::SFML::Update(this->window, elapsedTime);
    if (gameState.showDebugUI) {
        setupDebugUI(this->window);
    }

    // testing stuff
    sf::Rect<f32> playerBounds = this->player.playerAnimations[this->player.currentAnimation]->target.getGlobalBounds();
    this->playerHitbox = sf::RectangleShape(playerBounds.getSize());
    this->playerHitbox.setPosition(playerBounds.getPosition());
    this->playerHitbox.setFillColor(sf::Color(0, 0, 0, 0));
    this->playerHitbox.setOutlineThickness(2.0f);
    this->playerHitbox.setOutlineColor(sf::Color::Red);
    this->colisionTesterBounds = this->colisionTester.getGlobalBounds();
    if (checkColision(playerBounds, colisionTesterBounds)) {
        this->colisionTester.setFillColor(sf::Color::Green);
    }
    else {
        this->colisionTester.setFillColor(sf::Color::Blue);
    }
    #endif
}

void Game::render()
{
    this->window.clear();
        
    this->window.setView(this->regularView); // set regular view for UI
    this->parallax->draw(this->window);
    this->window.draw(sf::Text(std::to_string(gameState.fps), this->font)); // draw FPS on the screen
        
    this->window.setView(player.camera); // set camera
    this->window.draw(this->player.playerAnimations[this->player.currentAnimation]->target); // draw player current sprite
    
    #ifdef DEBUG
    // debug UI rendering
    ImGui::SFML::Render(window);

    this->window.draw(this->playerHitbox);
    this->window.draw(this->colisionTester);
    #endif

    this->window.display();
}

void Game::scale()
{
    sf::Vector2u windowSize = this->window.getSize();
    
    // this will scale based on the width
    u32 windowSizeFactor = windowSize.x;
    
    for (auto object : this->scalableObjects) {
        f32 scale = (f32) object.scaleFactor * (f32) windowSizeFactor;
        object.object->setScale(scale, scale);
    }

    // fix parallax background position
    f32 parallaxHeight = this->parallax->layers[0]->sprite.getGlobalBounds().height;
    f32 yParallaxPosition = (f32) windowSize.y - parallaxHeight;
    parallax->setPosition(sf::Vector2f(0.0f, yParallaxPosition));
}