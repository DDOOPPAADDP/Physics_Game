#pragma once

#include <fstream>
#include "Logging.h"
#include "Types.h"

struct InMemoryFile {
    void* content;
    size_t len;
};

// NOTE: Returns { nullptr, 0 } when fails
InMemoryFile loadFileInMemory(std::string filePath);


bool isSubstring(std::string firstStr, std::string otherStr);