#pragma once

#include <SFML/System.hpp>
#include "Types.h"

// player
inline const f32 PLAYER_SPEED_MOV = 330.0f;
inline const f32 PLAYER_JUMP_VELOCITY = 8.6f;
inline const sf::Vector2u PLAYER_SPRITE_SIZE = sf::Vector2u(120, 80);
inline const f32 PLAYER_SPRITE_SCALE_FACTOR = 0.002578;

// parallax background
inline const sf::Vector2i PARALLAX_SPRITE_SIZE = sf::Vector2i(272, 160);
inline const f32 PARALLAX_SCALE_FACTOR = 0.00375f;
inline const f32 PARALLAX_SCROLLING_SPEED = 0.0005f;

// physics
inline const f32 METERS_TO_WORLD_UNITS = 110.0f;
inline const f32 GRAVITY_ACCELERATION = 22.0f;