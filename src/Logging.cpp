#include "Logging.h"

static std::string getCurrentDateTime() {
    std::time_t t = std::time(nullptr);
    std::tm* now = std::localtime(&t);
 
    char buffer[128];
    strftime(buffer, sizeof(buffer), "%d-%m-%Y %X", now);
    return buffer;
}


void reportLog(const std::string message, LogLevel logLevel)
{
    switch (logLevel)
    {
        case INFO: {
            std::cout << getCurrentDateTime() << " [INFO]: " << message << "\n";
        } break;
        
        case WARN: {
            std::cout << getCurrentDateTime() << " [WARN]: " << message << "\n";
        } break;

        case ERROR: {
            std::cerr << getCurrentDateTime() << " [ERROR]: " << message << "\n";
        } break;

        case FATAL: {
            std::cerr << getCurrentDateTime() << " [FATAL]: " << message << "\n";
        } break;

        default: {
        } break;
    }
}