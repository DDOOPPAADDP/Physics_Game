#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "Types.h"

struct AnimationFrame {
    sf::IntRect rect;
    sf::Time duration;
    sf::Time timelinePoint;
};

/*
* NOTE: That is essentially a class, but since I don't like C++ black magic, I just called it a struct to avoid crazy shit.
* NOTE: Every time you switch an animation you must set 'frameIndex' to 0 or the animation will start in a random frame
*/
struct SpriteAnimation {
    sf::Sprite target;
    std::vector<AnimationFrame> frames;
    u32 frameIndex = 0;
    sf::Time currentProgress;
    sf::Time totalLength;
    b8 isFlipped;

    SpriteAnimation();
    //~SpriteAnimation();
    void addFrame(AnimationFrame&& frame);
    void update(sf::Time elapsedTime);
};