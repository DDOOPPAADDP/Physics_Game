#pragma once

#include <ctime>
#include <iostream>

enum LogLevel {
    INFO,
    WARN,
    ERROR,
    FATAL
};

// TODO: add colorful logging support
void reportLog(const std::string message, LogLevel logLevel);