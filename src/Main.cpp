#include <iostream>
#include <SFML/Graphics.hpp>
#ifdef DEBUG
#include <imgui-SFML.h>
#endif
#include "GameState.h"
#include "Game.h"


int main(int argc, char** argv)
{
    std::cout << "hello world!\n";

    Game gameApp;
    gameApp.initialize();

    sf::Event event; // stores the events received by the window 
    sf::Clock frameClock; // calculates the duration of every frame
    while (gameApp.window.isOpen()) {
        // handle events 
        #ifdef DEBUG
        ImGui::SFML::ProcessEvent(gameApp.window, event);
        #endif
        gameApp.window.pollEvent(event);
        switch (event.type) {
            case sf::Event::Closed: {
                gameApp.window.close();
            } break;

            case sf::Event::Resized: {
                gameApp.scale();
            }
            
            default: {
                #ifdef _WIN32
                static sf::Vector2u lastWindowSize;
                sf::Vector2u currentWindowSize = gameApp.window.getSize();

                if (currentWindowSize != lastWindowSize) {
                    gameApp.scale();
                }
                #endif
            } break;
        }

        // timing stuff
        sf::Time elapsedTime = frameClock.restart();
        gameState.delta = elapsedTime.asSeconds();
        gameState.fps = 1.0f / gameState.delta;

        // game logic && rendering
        gameApp.update(elapsedTime);
        gameApp.render();
    }

    return 0;
}
