#pragma once

#include <filesystem>
#include <SFML/Graphics.hpp>
#include "Animation.h"
#include "Background.h"
#include "Utils.h"
#include "Logging.h"

/*
* NOTE: sf::Texture && SpriteAnimation are dynamically allocated use 'unloadAnimatedSprite()' to free
* TODO: Transform that into an object that will keep track of all assets (using a arena allocator)
*/

SpriteAnimation* loadAnimatedSprite(std::string filePath, sf::Vector2u dimensions, f32 frameTime);
SpriteAnimation* loadAnimatedSprite(std::string filePath, sf::Vector2u dimensions, f32 frameTimes[512]); // maybe I should switch that to a vector but for now it's fine

// TODO: Implement that (this is going to load assets from a .pak file, and the offsets will be computed at compile time)
SpriteAnimation* loadAnimatedSprite(size_t offset, sf::Vector2u dimensions, f32 frameTime);
SpriteAnimation* loadAnimatedSprite(size_t offset, sf::Vector2u dimensions, f32 frameTimes[512]);

void unloadAnimatedSprite(SpriteAnimation* spriteAnimation);


// NOTE: ParallaxBackground is dynamically allocated use 'unloadParallaxBackground()' to free
ParallaxBackground* loadParallaxBackgroundFromDirectory(std::string path); 

void unloadParallaxBackground(ParallaxBackground* background);