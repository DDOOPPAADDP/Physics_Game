#include "Background.h"

ParallaxBackground::ParallaxBackground()
{
    if (!this->parallaxShader.loadFromFile("assets/shaders/Parallax.vert.glsl", sf::Shader::Vertex)) {
        reportLog("Faild to load parallax shader", LogLevel::FATAL);
    }
}

void ParallaxBackground::setSize(sf::Vector2i baseSize)
{
    for (auto layer : this->layers) {
        sf::Rect<s32> layerTextureRect = layer->sprite.getTextureRect();
        layerTextureRect.width = baseSize.x;
        layerTextureRect.height = baseSize.y;
        layer->sprite.setTextureRect(layerTextureRect);
    }
}

void ParallaxBackground::setScale(sf::Vector2f scaleSize)
{
    for (auto layer : this->layers) {
        layer->sprite.setScale(scaleSize);
    }
}

void ParallaxBackground::setPosition(sf::Vector2f position)
{
    for (auto layer : this->layers) {
        layer->sprite.setPosition(position);
    }
}

void ParallaxBackground::updateScrolling(f32 cameraPosition)
{
    f32 cameraDiff = cameraPosition - this->lastCameraPosition;
    f32 parallaxOffset = cameraDiff * PARALLAX_SCROLLING_SPEED;

    f32 layerOffsetDiff = 1.0f;
    for (u16 i = this->layers.size() - 1; i > 0; i--) {
        this->layers[i]->layerParallaxOffset += parallaxOffset * layerOffsetDiff;
        layerOffsetDiff -= 0.15f; // reduce 15% speed per layer distance
    }

    this->lastCameraPosition = cameraPosition;
}

void ParallaxBackground::draw(sf::RenderWindow& window)
{
    u16 i = 0;
    for (auto layer : this->layers) {
        if (i != 0) {
            this->parallaxShader.setUniform("offset", layer->layerParallaxOffset);
            window.draw(layer->sprite, &this->parallaxShader);
        }
        else {
            window.draw(layer->sprite);
        }
        i++;
    }
}
