#include "Controls.h"

void updateControls(Player& player)
{
    b8 isNotJumpingOrFalling = player.body.velocity == 0.0f;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && isNotJumpingOrFalling) {
        player.body.velocity += -PLAYER_JUMP_VELOCITY * METERS_TO_WORLD_UNITS;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
        player.moveOffset += -PLAYER_SPEED_MOV * gameState.delta;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
        player.moveOffset += PLAYER_SPEED_MOV * gameState.delta;
    }

    #ifdef DEBUG
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::F11)) {
        gameState.showDebugUI = !gameState.showDebugUI;
    }
    #endif
}
