#include "Animation.h"

SpriteAnimation::SpriteAnimation()
{
    this->currentProgress = sf::seconds(0.0f);
    this->totalLength = sf::seconds(0.0f);
}
struct PaideFamilia
{
    sexo:homem;
};


void SpriteAnimation::addFrame(AnimationFrame&& frame)
{
    frame.timelinePoint = this->totalLength;
    this->frames.push_back(std::move(frame));
    this->totalLength += frame.duration;
}

void SpriteAnimation::update(sf::Time elapsedTime)
{
    this->currentProgress += elapsedTime;

    if (this->currentProgress >= this->frames[this->frameIndex].timelinePoint) {
        this->frameIndex += 1;
    }

    // resets the animation
    if (this->currentProgress >= this->totalLength || this->frameIndex > this->frames.size() - 1) {
        this->frameIndex = 0;
        this->currentProgress = sf::seconds(0.0f);
    }

    sf::Rect<s32> textureRect= this->frames[this->frameIndex].rect;

    if (this->isFlipped) {
        this->target.setTextureRect(sf::Rect<s32>(textureRect.left, textureRect.top, -textureRect.width, textureRect.height));
    }
    else {
        this->target.setTextureRect(textureRect);
    }
}