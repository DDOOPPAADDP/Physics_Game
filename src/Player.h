#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "Animation.h"
#include "Physics.h"
#include "Defines.h"

// NOTE: The real max is (MAX_PLAYER_ANIMATIONS - 1)
enum PlayerAnimations {
    IDLE,
    RUN,
    JUMP,
    FALL,
    MAX_PLAYER_ANIMATIONS
};

enum DirectionState {
    LEFT = -1,
    RIGHT = 1
};

enum JumpState {
    NONE,
    JUMPING,
    FALLING
};

/*
* NOTE: The player position must be in world coordinates
* NOTE: 'updateCurrentAnimation()' must be called before 'updatePosition()'
*/
struct Player {
    // sprite animation
    PlayerAnimations currentAnimation = PlayerAnimations::IDLE; // defaults to IDLE
    SpriteAnimation* playerAnimations[PlayerAnimations::MAX_PLAYER_ANIMATIONS];
    b8 isSpriteFlipped = false;
    // movement
    sf::Vector2f position;
    f32 moveOffset; // only X axis
    PhysicsBody body;
    // camera
    sf::View camera;
    
    void updatePosition(std::vector<sf::Rect<f32>*> colisionTiles);
    void updateCurrentAnimation(sf::Time elapsedTime);
    void updateCamera(sf::RenderWindow& window);
};
