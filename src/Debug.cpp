#include "Debug.h"

static std::vector<sf::VideoMode> videoModes;
static std::vector<std::string> videoModesStr;

void setupDebugUI(sf::RenderWindow& window)
{
    if (!(videoModes.size() > 0 || videoModesStr.size() > 0)) {
        videoModes = sf::VideoMode::getFullscreenModes();
        u32 i = 0;
        for (auto mode : videoModes) {
            std::string str = std::to_string(mode.width) + "X" + std::to_string(mode.height);
            videoModesStr.push_back(str);
            i++;
        }
    }

    static u32 currentModeIndex = 0;
    static u32 lastModeIndex = 0;
    static b8 lastFullscreen = false;

    ImGui::Begin("Configurations"); {
        const c8* modePreview = videoModesStr[currentModeIndex].c_str();
        if (ImGui::BeginCombo("Pick Resolution", modePreview, 0)) {
            for (u32 i = 0; i < videoModes.size(); i++) {
                const b8 isSelected = (currentModeIndex == i);
                if (ImGui::Selectable(videoModesStr[i].c_str(), isSelected)) {
                    currentModeIndex = i;
                }
                if (isSelected) {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndCombo();
        }
        ImGui::Checkbox("Fullscreen", &gameState.isFullscreen);
    } ImGui::End();

    if (lastModeIndex != currentModeIndex || gameState.isFullscreen != lastFullscreen) {
        u32 windowStyle;
        if (gameState.isFullscreen) {
            windowStyle = sf::Style::Fullscreen;
        }
        else {
            windowStyle = sf::Style::Close;
        }
        
        window.create(videoModes[currentModeIndex], gameState.windowTitle, windowStyle);
        
        lastModeIndex = currentModeIndex;
        lastFullscreen = gameState.isFullscreen;
    }
}