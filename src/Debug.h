#pragma once

#include <SFML/Graphics.hpp>
#include <imgui.h>
#include "GameState.h"
#include "Types.h"

void setupDebugUI(sf::RenderWindow& window);