#pragma once

#include <string>
#include "Types.h"

struct GameState {
    std::string windowTitle;
    b8 isFullscreen;
    f32 delta;
    f32 fps;
    #ifdef DEBUG
    b8 showDebugUI;
    #endif
};

inline GameState gameState = {
    .windowTitle = "X-lab",
    .isFullscreen = false, // defaults to false
    .delta = 0.0f,
    .fps = 0.0f,
    #ifdef DEBUG
    .showDebugUI = true,
    #endif
};