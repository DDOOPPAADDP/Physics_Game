#include "Player.h"

void Player::updatePosition(std::vector<sf::Rect<f32>*> colisionTiles)
{
    // update body props
    this->body.rect = this->playerAnimations[this->currentAnimation]->target.getGlobalBounds(); // this is needed to get the size of the player
    this->body.rect.left = this->position.x;
    this->body.rect.top = this->position.y;
    
    updatePhysics(&this->body, colisionTiles);
    this->position.x += this->moveOffset;
    this->position.y = body.rect.top;
    this->moveOffset = 0.0f;
    this->playerAnimations[this->currentAnimation]->target.setPosition(this->position);
}

void Player::updateCurrentAnimation(sf::Time elapsedTime)
{
    // set the direction independently
    if (moveOffset > 0.0f) {
        this->isSpriteFlipped = false;
    }
    else if (moveOffset < 0.0f) {
        this->isSpriteFlipped = true;
    }

    // set the animation to play
    if (this->body.velocity != 0.0f) { // is jumping or falling
        if (this->body.velocity < 0.0f) {
            this->currentAnimation = PlayerAnimations::JUMP;
        }
        else {
            this->currentAnimation = PlayerAnimations::FALL;
        }
    }
    else if (this->moveOffset != 0.0f) { // is running
        this->currentAnimation = PlayerAnimations::RUN;
    }
    else { // is idle
        this->currentAnimation = PlayerAnimations::IDLE;
    }

    this->playerAnimations[this->currentAnimation]->isFlipped = this->isSpriteFlipped;
    this->playerAnimations[this->currentAnimation]->update(elapsedTime);
}

void Player::updateCamera(sf::RenderWindow& window)
{
    this->camera.setSize(static_cast<sf::Vector2f>(window.getSize())); // this may cause performance issues
    this->camera.setCenter(sf::Vector2f(this->position.x + this->body.rect.width * 0.5f, this->position.y + this->body.rect.height * 0.5f));
}