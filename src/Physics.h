#pragma once

#include <SFML/Graphics.hpp>
#include "GameState.h"
#include "Defines.h"
#include "Types.h"

b8 checkColision(sf::Rect<f32> rect, sf::Rect<f32> otherRect);
b8 checkColision(sf::Rect<f32> rect, sf::Rect<f32> otherRect, sf::Rect<f32>& intersection);

struct PhysicsBody {
    sf::Rect<f32> rect;
    f32 velocity;
};

void updatePhysics(PhysicsBody* body, std::vector<sf::Rect<f32>*> colisionTiles);