#include "AssetLoading.h"

static SpriteAnimation* loadAnimationSpritesheet(std::string filePath)
{
    SpriteAnimation* spritesheet = new SpriteAnimation();
    sf::Texture* spriteTexture = new sf::Texture();
    if (!spriteTexture->loadFromFile(filePath)) {
        reportLog("Error trying to load spritesheet", LogLevel::FATAL);
        exit(EXIT_FAILURE); 
    }
    spritesheet->target = sf::Sprite(*spriteTexture);
    
    return spritesheet;
}

SpriteAnimation* loadAnimatedSprite(std::string filePath, sf::Vector2u dimensions, f32 frameTime)
{
    SpriteAnimation* spriteAnimation = loadAnimationSpritesheet(filePath);
    u32 totalFrames = spriteAnimation->target.getTexture()->getSize().x / dimensions.x;

    u32 offset = 0;
    for (u32 i = 0; i < totalFrames; i++) {
        spriteAnimation->addFrame({ sf::IntRect(offset, 0, dimensions.x, dimensions.y), sf::seconds(frameTime) });
        offset += dimensions.x; 
    }

    return spriteAnimation;
}

SpriteAnimation* loadAnimatedSprite(std::string filePath, sf::Vector2u dimensions, f32 frameTimes[512])
{
    SpriteAnimation* spriteAnimation = loadAnimationSpritesheet(filePath);
    u32 totalFrames = spriteAnimation->target.getTexture()->getSize().x / dimensions.x;

    u32 offset = 0;
    for (u32 i = 0; i < totalFrames; i++) {
        spriteAnimation->addFrame({ sf::IntRect(offset, 0, dimensions.x, dimensions.y), sf::seconds(frameTimes[i]) });
        offset += dimensions.x; 
    }

    return spriteAnimation;
}

void unloadAnimatedSprite(SpriteAnimation* spriteAnimation)
{
    delete spriteAnimation->target.getTexture(); // free the texture
    delete spriteAnimation; // free sprite
}

ParallaxBackground* loadParallaxBackgroundFromDirectory(std::string path)
{
    // FIXME: For some stupid reason c++ doesn't allow me to allocate this on the stack
    ParallaxBackground* parallaxBackground = new ParallaxBackground();

    std::vector<std::string> fileList;
    for (const auto& entry : std::filesystem::directory_iterator(path)) {
        std::string entryPath = entry.path().string();
        if (isSubstring(entryPath, "parallax")) {
            fileList.push_back(entryPath);
        }
    }
    // this will load the layers in order
    u32 i = 0;
    while (fileList.size() > parallaxBackground->layers.size()) {
        for (const auto filePath : fileList) {
            if (isSubstring(filePath, std::to_string(parallaxBackground->layers.size()))) {
                ParallaxLayer* currentLayer = new ParallaxLayer();
                if (!currentLayer->texture.loadFromFile(filePath)) {
                    reportLog("Error trying to load background layer", LogLevel::FATAL);
                    exit(EXIT_FAILURE); 
                }
                currentLayer->texture.setRepeated(true);
                currentLayer->sprite = sf::Sprite(currentLayer->texture);
                parallaxBackground->layers.push_back(currentLayer);

                break; // break here to save iterations
            }
        }
        if (i > 10) { // fail if there's more than 10 layers or if couldn't find the right files
            reportLog("Error trying to load more than 10 background layers or the files aren't numerated", LogLevel::FATAL);
            exit(EXIT_FAILURE); 
        }
        i++;
    }

    return parallaxBackground;
}