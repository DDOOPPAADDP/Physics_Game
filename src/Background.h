#pragma once

#include <SFML/Graphics.hpp>
#include "Logging.h"
#include "Defines.h"
#include "Types.h"

struct ParallaxLayer {
    sf::Texture texture;
    sf::Sprite sprite;

    f32 layerParallaxOffset;
};

// NOTE: CameraPosition refers only to X axis
struct ParallaxBackground {
    std::vector<ParallaxLayer*> layers;
    f32 lastCameraPosition;
    sf::Shader parallaxShader;

    ParallaxBackground();
    void setSize(sf::Vector2i baseSize);
    void setScale(sf::Vector2f scaleSize);
    void setPosition(sf::Vector2f position);
    void updateScrolling(f32 cameraPosition);
    void draw(sf::RenderWindow& window);
};
