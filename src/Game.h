#pragma once

#include <SFML/Graphics.hpp>
#include "Types.h"
#include "GameState.h"
#include "Defines.h"
#include "Controls.h"
#include "Player.h"
#include "Physics.h"
#include "Background.h"
#include "Logging.h"
#include "AssetLoading.h"
#ifdef DEBUG
#include <imgui-SFML.h>
#include "Debug.h"
#endif

struct ScalableObject {
    sf::Transformable* object;
    f32 scaleFactor;
};;

// this will basically contain everything that is necessery for the game to work
struct Game {
    sf::RenderWindow window;
    sf::View regularView; // this will be used to render UI elements and stuff like that
    
    sf::Font font;

    Player player;

    ParallaxBackground* parallax;

    std::vector<sf::Rect<f32>*> colisionTiles;

    std::vector<ScalableObject> scalableObjects;

    // testing stuff
    #ifdef DEBUG
    sf::RectangleShape colisionTester;
    sf::Rect<f32> colisionTesterBounds;
    sf::RectangleShape playerHitbox;
    #endif


    void initialize();
    void update(sf::Time elapsedTime);
    void render();
    void scale();
};