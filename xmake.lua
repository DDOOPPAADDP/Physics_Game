set_languages("cxx20")
add_rules("mode.debug", "mode.release")
add_requires("sfml", "imgui-sfml")

if is_mode("debug") then 
    add_defines("DEBUG")
end

target("sfml_game")
    set_kind("binary")
    add_files("src/*.cpp")
    add_packages("sfml", "imgui-sfml")
target_end()